// Gets a handle to the element with id canvasOne.
var canvas = document.getElementById("canvas-for-ball");
// Get a 2D context for the canvas.
var ctx = canvas.getContext("2d");

// resize canvas
const maxX = 250
const maxY = 550
canvas.width = maxX
canvas.height = maxY

// The starting poition of the ball.
let pos = { x:50, y: 10 }

/** ADVANCE PART */

class Beachball{
    constructor({
        x,                              // X position of the centre point
        y,                              // Y position of the centre point
        position,                       // X & Y position of the centre point
        colorRGB = 'rgb(241, 151, 17)', // color of the beachball
        radius = 50,                    // radius of the beachball
        segments = 7,                   // segments on the beach ball
        velX = 2,                       // velocit X direction
        velY = 3}){                     // velocity Y direction

        if( position ){
            let {x, y} = position
            this.x = x; this.y = y
        }else{
            this.x = x; this.y = y
        }

        this.radius = radius
        this.color = colorRGB
        this.segments = segments
        this.velX = velX
        this.velY = velY
        this.spin = 1
        this.velSpin = 0.05
    }

    /**
    *   Draws ball on a canvas
    */
    draw(){
        ctx.beginPath()
        ctx.fillStyle = this.color
        ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI)
        ctx.fill()

        // segment angle (angle between two lines)
        let angle = 2 *Math.PI / this.segments
        // draw segments
        for( let i = 1; i <= this.segments; i++){
            ctx.moveTo(this.x, this.y)
            ctx.lineTo(
                this.x +this.radius * Math.cos(angle *i +this.spin),
                this.y +this.radius * Math.sin(angle *i +this.spin) )
        }

        ctx.stroke()
    }

    /**
    *   Moves ball inside given limits
    */
    move({maxX, maxY}){
        // update position
        this.y += this.velY
        this.x += this.velX
        // update spin
        this.spin += this.velSpin

        let offLimit = this.offLimits({maxX, maxY})

        if( offLimit ){
            /*
            Ball rotation should change if:
                x > 0 && y > 0 => off-x: rev, off-y: clock
                x < 0 && y < 0 => off-x: rev, off-y: clock
                x > 0 && y < 0 => off-x: clock, off-y: rev
                x < 0 && y > 0 => off-x: clock, off-y: rev
            */

            if( this.velX * this.velY > 0 ){
                switch( offLimit ){
                    case 'x':
                        if( this.velSpin > 0 ){ this.velSpin *= -1 }; break
                    case 'y':
                        if( this.velSpin < 0 ){ this.velSpin *= -1 }; break }

            }else {
                switch( offLimit ){
                    case 'x':
                        if( this.velSpin < 0 ){ this.velSpin *= -1 }; break
                    case 'y':
                        if( this.velSpin > 0 ){ this.velSpin *= -1 }; break }
            }

            // change movement direction
            if( offLimit === 'x' ){ this.velX *= -1 }
            if( offLimit === 'y' ){ this.velY *= -1 }

        }

    }

    /**
    *   Checks if ball are in given limits
    */
    offLimits({maxX, maxY}){
        if( this.x > maxX -this.radius || this.x < 0 +this.radius ){ return 'x'}
        if( this.y > maxY -this.radius || this.y < 0 +this.radius ){ return 'y'}
    }

}// class Beachball

// create instance of the ball
const bBall = new Beachball({position:{x:50, y:50}, radius: 25})

// A function to repeat every time the animation loops.
const repeatme = () => {
    ctx.clearRect(0, 0, maxX, maxY)
    bBall.draw()
    bBall.move({maxX, maxY})

    window.requestAnimationFrame(repeatme);
}

// Get the animation going.
repeatme();
